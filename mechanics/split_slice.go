package mechanics

// SplitSlice will split a slice into two parts: everything to the left and
// everything to the right of the provided index. Both parts will exclude the
// element indexed by i.
//
// If the index is outside the bounds of the slice, it should return one empty
// slice and one slice of everything (whether this is left or right depends on
// the index value, see test cases for clarification).
//
// If a slice is empty, nil should be returned instead of an empty initialized
// slice (i.e. don't return []int{}, instead return nil).
//
// For example, if given s=[1,2,3,4,5] and i=2, the slice will be split into
// two halves [1,2] and [4,5].
//
// For example, if given s=[1,2,3,4] and i=1, the slice will be split into two
// different size slices [1] and [3,4].
//
// For example, if given s=[1,2] and i=0, then the returned slices will be an
// empty slice and [2].
func SplitSlice(s []int, i int) (left, right []int) {
	// do your thing...
	return nil, nil
}
