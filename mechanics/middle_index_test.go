package mechanics

import "testing"

func TestMiddleElement(t *testing.T) {
	for _, tt := range []struct {
		input []int
		want  int
		err   bool
	}{
		{
			input: []int{0},
			want:  0,
		},
		{
			input: []int{0, 1},
			want:  0,
		},
		{
			input: []int{0, 1, 2},
			want:  1,
		},
		{
			input: []int{5, 3, 9, 2},
			want:  3,
		},
		{
			err: true,
		},
	} {
		actual, err := MiddleElement(tt.input)
		if (err != nil) != tt.err {
			t.Fatalf("err value not expected: %v", err)
		}

		if actual != tt.want {
			t.Logf("Input: %v", tt.input)
			t.Errorf(
				"expected middle to be %d but got %d",
				tt.want, actual,
			)
		}
	}
}
